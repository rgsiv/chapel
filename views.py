import json
import write_file
import execute
from flask import Flask
from flask import render_template,request

app=Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/output",methods=['POST'])
def resp():
    code=request.form['code'];
    write_file.main(code)
    execout=execute.main('file.chpl')
    return render_template('output.html',execout=execout)

if __name__=='__main__':
    app.run()
