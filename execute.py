import time
import subprocess
import sys

class execute(object):
    
    def __init__(self,filename):
        self.filename=filename

    def compile(self):
            p=subprocess.Popen([r"chpl",self.filename],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            p.communicate()
            p=subprocess.Popen([r"./a.out"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            a=subprocess.check_output([r"./a.out"]).decode(sys.stdout.encoding).strip()
            return a

def main(filename):
    obj=execute(filename)
    out=obj.compile()
    out.replace('\\n','\n')
    return out
