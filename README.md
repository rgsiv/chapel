#Chapel

Chapel is a simple Flask application that compiles Chapel code and returns the output.
To run the app :
python3 views.py

Dependencies :

Chapel
Python3
Flask framework for python3

Ubuntu users run ./req to install the dependencies.

To Do List :

1.Implement input feature ie allow users to enter custom input
2.Use docker to wrap all the dependencies and build a docker image
3.Serve Flask application with nginx and uwsgi
4.Extend the present docker image of Chapel.
5.Implement queuing using Celery

Link : http://139.59.79.155:5000/

